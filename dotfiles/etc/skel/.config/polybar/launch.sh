#!/bin/bash

# Terminate already running bar instances:
/usr/bin/polybar-msg cmd quit

for monitor in $(/usr/bin/polybar --list-monitors | cut -d ":" -f1); do
    MONITOR=$monitor /usr/bin/polybar --reload main & disown

done
