# Kali i3 | Custom ISO's dotfiles
## Disclaimer
As of 2023-01-27, `i3-gaps` is no longer maintained as it has finally merged 
with `i3`, bringing gaps to `i3` itself. Thus, this repository has been 
renamed to `i3-dotfiles` and will be maintained as such.

## Description
These are the custom dotfiles and scripts supplied on [Kali i3][Kali i3]. All 
of the files here work in unison, thus they are advised to be moved/applied 
together.

## List of Dotfiles
- `.gtkrc-2.0` for `lxappearance`
- `.Xresources` for `X11`
- `.config/mimeapps.list` for `mime`
- `.config/dunst/dunstrc` for `dunst`
- `.config/gtk-3.0/settings.ini` for  `lxappearance`
- `.config/i3/config` for `i3`/`i3-gaps`
- `.config/libfm/libfm.conf` for `pcmanfm`
- `.config/picom/picom.conf` for `picom`
- `.config/polybar/config.ini` for `polybar`
- `.config/ranger/rc.conf` for `ranger`
- `.config/rofi/config.rasi` for `rofi`

## List of Scripts
The scripts listed and installed here are called by `/etc/skel/.config/i3/config`
- `/usr/bin/i3exit` for handling system actions (lock, sleep, hibernate, etc.)
- `/usr/bin/brightnessControl` for handling `brightnessctl`
- `/usr/bin/volumecontrol` for handling `amixer`/`pulseaudio`

## FAQ
To disable quasi-altering (Fibonacci) windows, comment out the following line in
`.config/i3/config.d/execs.conf`:

```
exec --no-startup-id $HOME/.config/i3/alternating_layouts.py
```

This can be achieved via terminal with the following command:

```
$ sed -i '/alternating_layouts.py/s/^/#/g' $HOME/.config/i3/config.d/execs.conf
```

[Kali i3]: https://gitlab.com/Arszilla/kali-i3/
